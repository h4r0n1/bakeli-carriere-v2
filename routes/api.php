<?php

use App\Http\Controllers\API\UserApiController;
use App\Http\Controllers\AUTH\AuthController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
 */

Route::post('login', [AuthController::class, 'login']);

Route::middleware(['auth:sanctum'])->group(function () {
    // User routes
    Route::get('users', [UserApiController::class, 'index']);
    Route::post('add_user', [UserApiController::class, 'store']);
    Route::get('show_user/{id}', [UserApiController::class, 'show']);
    Route::post('edit_user/{id}', [UserApiController::class, 'update']);
    Route::delete('delete_user/{id}', [UserApiController::class, 'destroy']);

    // Auth protected routes
    Route::post('logout', [AuthController::class, 'logoutUser']);
    Route::get('current_user', [UserApiController::class, 'currentUser']);

});
