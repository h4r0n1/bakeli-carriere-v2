<?php

namespace App\Http\Controllers\AUTH;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{

    /**
     * Login User.
     *
     * @OA\Post(
     *      path="/api/login",
     *      operationId="login",
     *      tags={"Auth"},
     *      summary="Login",
     *      description="Login",
     *      @OA\Parameter(
     *      name="email",
     *      in="query",
     * ),
     *      @OA\Parameter(
     *      name="password",
     *      in="query",
     *      ),
     *        @OA\Response(
     *      response=200,
     *      description="Success",
     *     ),
     * )
     * @return \Illuminate\Http\Response
     */

    public function login(Request $request)
    {
        if (!Auth::attempt($request->only('email', 'password'))) {
            return response()->json([
                'message' => 'Email ou mot de passe incorrect.',
            ], 401);
        }

        $user = User::where('email', $request['email'])->firstOrFail();

        $token = $user->createToken('auth_token')->plainTextToken;

        return response()->json([
            'token' => $token,
            'token_type' => 'Bearer',
            "user" => $user,
        ], 201);

    }

    // Function that return the current user
    public function currentUser(Request $request)
    {
        if ($request) {
            $request->user();
        } else {
            return response()->json([
                'message' => 'Invalid login details',
            ], 401);

        }
    }

    /**
     * Login User.
     *
     * @OA\Post(
     *      path="/api/logout",
     *      operationId="logout",
     *      tags={"Auth"},
     *      summary="Logout",
     *      description="Logout",
     *        @OA\Response(
     *      response=200,
     *      description="Success",
     *     ),
     * )
     * @return \Illuminate\Http\Response
     */

    public function logoutUser(Request $request)
    {
        // Revoke the token that was used to authenticate the current request...
        // $request->user()->currentAccessToken()->delete();
        Auth::guard('web')->logout();

        return response()->json([
            'message' => 'Utilisateur déconnecté.',
        ]);

    }
}
