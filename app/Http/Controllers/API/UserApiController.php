<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class UserApiController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @OA\Get(
     *      path="/api/users",
     *      operationId="getUser",
     *      tags={"Users"},
     *      summary="Get all users",
     *      description="Get all users",
     *        @OA\Response(
     *      response=200,
     *      description="Success",
     *     )
     * )
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $users = User::all();

        if (!is_null($users)) {
            return response()->json([
                'success' => true,
                'message' => 'Utilisateurs trouvé.',
                'data' => $users,
            ], 200);

        }

    }

    /**
     * Store a newly created resource in storage.
     * @OA\Post(
     *      path="/api/add_user",
     *      operationId="addUser",
     *      tags={"Users"},
     *      summary="Add new user",
     *      description="Add new user",
     *      @OA\Parameter(
     *      name="name",
     *      in="query",
     *      required=true,
     *      @OA\Schema(
     *           type="string"
     *      ),
     *      ),
     *      @OA\Parameter(
     *      name="email",
     *      in="query",
     *      required=true,
     *      @OA\Schema(
     *           type="string"
     *      ),
     *      ),
     *      @OA\Parameter(
     *      name="password",
     *      required=true,
     *      in="query",
     *       @OA\Schema(
     *           type="string"
     *      ),
     *      ),
     *        @OA\Response(
     *      response=200,
     *      description="Success",
     *     )
     * )
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $userExist = User::where('email', $request->email)->first();

        $payload = [
            'name' => $request->name,
            'email' => $request->email,
            'password' => Hash::make($request->password),
            'api_token' => Str::random(60),
            'remember_token' => Str::random(10),
        ];

        if ($userExist) {
            return response()->json([
                'message' => 'Cet utilisateur existe deja',
            ], 200);
        } else {
            $newUser = new User($payload);

            if ($newUser->save()) {

                return response()->json([
                    'success' => true,
                    'message' => 'Utilisateur ajouté avec success.',
                    'data' => [
                        'name' => $newUser->name,
                        'id' => $newUser->id,
                        'email' => $newUser->email,
                        // 'auth_token' => $newUser->api_token,
                    ],
                ], 200);

            }

        }
    }

    /**
     * Show a user.
     * @OA\Get(
     *      path="/api/show_user/{user_id}",
     *      operationId="showUser",
     *      tags={"Users"},
     *      summary="Show user",
     *      description="Show user",
     *      @OA\Parameter(
     *      name="user_id",
     *      required=true,
     *      in="path",
     *       @OA\Schema(
     *           type="number"
     *                  ),
     *      ),
     *        @OA\Response(
     *      response=200,
     *      description="Success",
     *     )
     *      )
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show($user_id)
    {
        $user = User::find($user_id);

        if (!$user) {
            return response()->json([
                'success' => true,
                'message' => 'Utilisateur non trouvé.',
            ], 404);

        } else {
            return response()->json([
                'success' => true,
                'message' => 'Utilisateur trouvé.',
                'data' => $user,
            ], 200);

        }
    }

    /**
     * Update the specified resource in storage.
     * @OA\Post(
     *      path="/api/edit_user/{user_id}",
     *      operationId="updateUser",
     *      tags={"Users"},
     *      summary="Edit user",
     *      description="Edit user",
     *      @OA\Parameter(
     *      name="name",
     *      in="query",
     *      @OA\Schema(
     *           type="string"
     *      ),
     *      ),
     *      @OA\Parameter(
     *      name="email",
     *      in="query",
     *      @OA\Schema(
     *           type="string"
     *      ),
     *      ),
     *      @OA\Parameter(
     *      name="password",
     *      in="query",
     *      @OA\Schema(
     *           type="string"
     *      ),
     *      ),
     *      @OA\Parameter(
     *      name="user_id",
     *      required=true,
     *      in="path",
     *      @OA\Schema(
     *           type="integer"
     *      ),
     *      ),
     *        @OA\Response(
     *      response=200,
     *      description="Success",
     *     )
     * )
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $userData = User::find(($id));

        if (!$userData) {
            return response()->json([
                'success' => true,
                'message' => 'Utilisateur non trouvé.',
            ], 404);

        } else {
            $userData->name = $request->name;
            $userData->email = $request->email;
            $userData->password = Hash::make($request->password);
        }

        if ($userData->save()) {
            return response()->json([
                'success' => true,
                'message' => 'Utilisateur modifié avec success.',
                'data' => $userData,
            ], 200);

        } else {
            return response()->json([
                'success' => true,
                'message' => 'Erreur lors de la modification.',
            ], 500);

        }

    }

    /**
     * Remove the specified resource from storage.
     * @OA\Delete(
     *      path="/api/delete_user/{user_id}",
     *      operationId="deleteUser",
     *      tags={"Users"},
     *      summary="Delete user",
     *      description="Delete user",
     *      @OA\Parameter(
     *      name="user_id",
     *      required=true,
     *      in="path",
     *      @OA\Schema(
     *           type="integer"
     *      ),
     *      ),
     *        @OA\Response(
     *      response=200,
     *      description="Success",
     *     )
     * )
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::find($id);

        if (!$user) {
            return response()->json([
                'success' => true,
                'message' => 'Utilisateur non trouvé.',
            ], 404);

        } elseif ($user->delete($user)) {
            return response()->json([
                'success' => true,
                'message' => 'Utilisateur supprimé avec success.',
            ], 200);

        } else {
            return response()->json([
                'success' => true,
                'message' => 'Erreur lors de la suppression',
            ], 500);

        }
    }

}
